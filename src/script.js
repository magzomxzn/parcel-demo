const calcButton = document.querySelector('#button');
const inputA = document.querySelector('#inputA');
const inputB = document.querySelector('#inputB');
const resultsList = document.querySelector('#results');

// false, undefined, null, '', 0
// true, {}, function, []

function sum(a, b) {
  return a + b;
}

const handleButtonClick = () => {
  const valueA = inputA.value;
  const valueB = inputB.value;
  const result = sum(+valueA, +valueB);
  console.log(result);

  const resultNode = document.createElement('li');
  resultNode.innerHTML = `<b>${result}</b>`;
  resultsList.appendChild(resultNode);
  inputA.value = '';
  inputB.value = '';
};

calcButton.addEventListener('click', handleButtonClick);
